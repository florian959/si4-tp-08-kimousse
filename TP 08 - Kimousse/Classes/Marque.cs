﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP_08___Kimousse.Classes
{
    class Marque
    {
        public int Id { get; set; }
        public string Nom { get; set; }
        public int IdBrasserie { get; set; }
    }
}
