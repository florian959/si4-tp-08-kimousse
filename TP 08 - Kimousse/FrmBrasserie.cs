﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using TP_08___Kimousse.Classes;
using Region = TP_08___Kimousse.Classes.Region;

namespace TP_08___Kimousse
{
    public partial class FrmBrasserie : Form
    {
        string connectionString;
        List<Brasserie> brasseries;
        List<Region> regions;

        public FrmBrasserie()
        {
            InitializeComponent();
        }

        private void FrmBrasserie_Load(object sender, EventArgs e)
        {
            txtNom.ReadOnly = false;
            txtVille.ReadOnly = false;
            cmbRegion.Enabled = false;
            connectionString = "Server=127.0.0.1;" +
                                "Port=3306;" +
                                "Persist Security Info=False;" +
                                "CharSet=utf8;" +
                                "User Id=kimousse-si4;" +
                                "Password=rR0BooeRXrFkdCYp;" +
                                "Database=kimousse-si4;";

            brasseries = new List<Brasserie>();
            regions = new List<Region>();

            // connexion
            MySqlConnection connection = new MySqlConnection();
            connection.ConnectionString = connectionString;
            try
            {
                connection.Open();
            }
            catch(MySqlException ex)
            {
                MessageBox.Show("Erreur de connexion à la base de données." + ex.Message, "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Application.Exit();
            }

            // chargement des brasseries
            MySqlCommand cmd = connection.CreateCommand();
            cmd.CommandText = @"SELECT * FROM `brasserie`;";
            cmd.Prepare();
            MySqlDataReader reader = cmd.ExecuteReader();

            while(reader.Read())
            {
                int _id;
                string _nom;
                string _ville;
                int _id_region;

                _id = reader.GetInt32("id");
                _nom = reader.GetString("nom");
                if (reader.IsDBNull(2))
                {
                    _ville = null;
                }
                else
                {
                    _ville = reader.GetString("ville");
                }
                _id_region = reader.GetInt32("id_region");

                Brasserie item = new Brasserie()
                {
                    Id = _id, Nom = _nom, Ville = _ville, IdRegion = _id_region
                };

                brasseries.Add(item);
            }
            reader.Close();

            lstBrasserie.DisplayMember = "nom";
            lstBrasserie.DataSource = brasseries;

            // chargement des régions
            cmd = connection.CreateCommand();
            cmd.CommandText = @"SELECT * FROM `region`;";
            cmd.Prepare();
            reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                int _id;
                string _nom;
                int _id_pays;

                _id = reader.GetInt32("id");
                _nom = reader.GetString("nom");
                _id_pays = reader.GetInt32("id_pays");

                Region item = new Region()
                {
                    Id = _id,
                    Nom = _nom,
                    IdPays = _id_pays
                };

                regions.Add(item);
            }
            reader.Close();

            cmbRegion.DisplayMember = "nom";
            cmbRegion.DataSource = regions;
        }

        private void lstBrasserie_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lstBrasserie.DataSource != null)
            {
                Brasserie brasserie = (Brasserie)lstBrasserie.SelectedItem;
                txtId.Text = brasserie.Id.ToString();
                txtNom.Text = brasserie.Nom;
                txtVille.Text = brasserie.Ville;
                Region region = regions.Find(r => r.Id == brasserie.IdRegion);
                cmbRegion.SelectedItem = region;
            }
        }

        private void btnNouveau_Click(object sender, EventArgs e)
        {
            txtId.Text = "";
            txtNom.Text = "";
            txtVille.Text = "";
            cmbRegion.Text = "";
            txtNom.ReadOnly = false;
            txtVille.ReadOnly = false;
            cmbRegion.Enabled = true;
            btnModifier.Enabled = false;
            btnSupprimer.Enabled = false;
        }

        private void btnAjouter_Click(object sender, EventArgs e)
        {
            connectionString = "Server=127.0.0.1;" +
                                           "Port=3306;" +
                                           "Persist Security Info=False;" +
                                           "CharSet=utf8;" +
                                           "User Id=kimousse-si4;" +
                                           "Password=rR0BooeRXrFkdCYp;" +
                                           "Database=kimousse-si4;";
            MySqlConnection connection = new MySqlConnection();
            connection.ConnectionString = connectionString;
            try
            {
                connection.Open();
            }
            catch (MySqlException ex)
            {
                MessageBox.Show("Erreur de connexion à la base de données." + ex.Message, "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Application.Exit();
            }

            MySqlCommand cmd = connection.CreateCommand();
            cmd = connection.CreateCommand();

            Region region = regions.Find(r => r.Nom == cmbRegion.Text);
            
            cmd.CommandText = @"INSERT INTO brasserie(nom,ville,id_region) VALUES(@nom_brasserie, @ville_brasserie, @id_region_brasserie);";
            cmd.Parameters.Add(new MySqlParameter("nom_brasserie", txtNom.Text));
            cmd.Parameters.Add(new MySqlParameter("ville_brasserie", txtVille.Text));
            cmd.Parameters.Add(new MySqlParameter("id_region_brasserie", region.Id));

            cmd.Prepare();
            MessageBox.Show(cmd.ExecuteNonQuery().ToString() + " requête(s) exécutée(s)");
            
            connection.Close();

            btnModifier.Enabled = true;
            btnSupprimer.Enabled = true;

            // todo 05 : mettre à jour la liste déroulante des brasseries
            /*brasseries = new List<Brasserie>();
            Brasserie item = new Brasserie()
            {
                Nom = txtNom.Text,
            };

            regions.Add(item);*/
        }

        private void btnModifier_Click(object sender, EventArgs e)
        {
            connectionString = "Server=127.0.0.1;" +
                                           "Port=3306;" +
                                           "Persist Security Info=False;" +
                                           "CharSet=utf8;" +
                                           "User Id=kimousse-si4;" +
                                           "Password=rR0BooeRXrFkdCYp;" +
                                           "Database=kimousse-si4;";
            MySqlConnection connection = new MySqlConnection();
            connection.ConnectionString = connectionString;
            try
            {
                connection.Open();
            }
            catch (MySqlException ex)
            {
                MessageBox.Show("Erreur de connexion à la base de données." + ex.Message, "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Application.Exit();
            }

            MySqlCommand cmd = connection.CreateCommand();
            cmd = connection.CreateCommand();

            Region region = regions.Find(r => r.Nom == cmbRegion.Text);

            cmd.CommandText = @"UPDATE brasserie
            SET nom = @nom_brasserie, ville = @ville_brasserie, id_region = @id_region_brasserie
            WHERE id = @id_brasserie;";
            cmd.Parameters.Add(new MySqlParameter("id_brasserie", Int32.Parse(txtId.Text)));
            cmd.Parameters.Add(new MySqlParameter("nom_brasserie", txtNom.Text));
            cmd.Parameters.Add(new MySqlParameter("ville_brasserie", txtVille.Text));
            cmd.Parameters.Add(new MySqlParameter("id_region_brasserie", region.Id));

            cmd.Prepare();
            MessageBox.Show(cmd.ExecuteNonQuery().ToString() + " requête(s) exécutée(s)");

            connection.Close();

            // todo 07 : mettre à jour la liste déroulante des brasseries
        }

        private void btnSupprimer_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Etes vous sûr ?", "Demande de confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == System.Windows.Forms.DialogResult.Yes)
            {
                connectionString = "Server=127.0.0.1;" +
                                           "Port=3306;" +
                                           "Persist Security Info=False;" +
                                           "CharSet=utf8;" +
                                           "User Id=kimousse-si4;" +
                                           "Password=rR0BooeRXrFkdCYp;" +
                                           "Database=kimousse-si4;";
                MySqlConnection connection = new MySqlConnection();
                connection.ConnectionString = connectionString;
                try
                {
                    connection.Open();
                }
                catch (MySqlException ex)
                {
                    MessageBox.Show("Erreur de connexion à la base de données." + ex.Message, "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Application.Exit();
                }

                MySqlCommand cmd = connection.CreateCommand();
                cmd = connection.CreateCommand();

                Region region = regions.Find(r => r.Nom == cmbRegion.Text);

                cmd.CommandText = @"DELETE FROM brasserie
                WHERE id = @id_brasserie;";
                cmd.Parameters.Add(new MySqlParameter("id_brasserie", Int32.Parse(txtId.Text)));

                cmd.Prepare();
                MessageBox.Show(cmd.ExecuteNonQuery().ToString() + " requête(s) exécutée(s)");

                connection.Close();
            }

            // todo 10 : mettre à jour la liste déroulante des brasseries
        }

        private void btnQuitter_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void cmbRegion_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtNom.ReadOnly = false;
            txtVille.ReadOnly = false;
            cmbRegion.Enabled = true;
        }
    }
}
